class Mamilos
	def self.show
		puts "Quais desses mamilos quer ver?"
		mamilos_index = 0
		lista = self.pega_os_mamilos.each do |m|
			mamilos_index = mamilos_index + 1
			puts "Digite #{mamilos_index} para ver o mamilo: #{File.basename(m)}"
		end

		resultado = gets.to_i - 1

		self.pega_o_mamilo(lista[resultado.to_i])
	end

	def self.pega_os_mamilos
		Dir[File.expand_path("#{self.root_path}/lib/txt/*.txt")]
	end

	def self.pega_o_mamilo(path)
		self.mostra_o_mamilo(File.open(path).read)
	end

	def self.mostra_o_mamilo(mamilo)
		print mamilo
	end

	def self.root_path
		Gem::Specification.find_by_name('mamilos').gem_dir
	end
end