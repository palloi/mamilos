Gem::Specification.new do |s|
	s.name = 'mamilos'
	s.version = '0.0.5'
	s.date = '2013-03-06'
	s.description = 'Mostrando os mamilos de arquivos txts =)'
	s.summary = 'Mostrando os mamilos =)'
	s.authors = ['Palloi Hofmann']
	s.email = 'palloi.hofmann@gmail.com'
	s.files = Dir['lib/*.rb', 'lib/txt/*.txt']
	s.homepage = 'https://bitbucket.org/palloi/mamilos'
end